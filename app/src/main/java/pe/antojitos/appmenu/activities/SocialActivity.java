package pe.antojitos.appmenu.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;

import pe.antojitos.appmenu.R;
import pe.antojitos.appmenu.adapters.SocialAdapter;
import pe.antojitos.appmenu.models.Social;

public class SocialActivity extends AppCompatActivity {

    private SocialAdapter socialAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<Social> lstSocial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social);
        BuildRecyclerView();
        setToolbar();
    }

    public void BuildRecyclerView(){

        lstSocial = this.getAllComment();
        RecyclerView myrv = (RecyclerView) findViewById(R.id.social_recyclerview_id);
        socialAdapter = new SocialAdapter(lstSocial, R.layout.item_social, this);
        myrv.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        myrv.setLayoutManager(layoutManager);
        myrv.setAdapter(socialAdapter);
    }

    public void setToolbar(){
        Toolbar mytoolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(mytoolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private ArrayList<Social> getAllComment(){
        return new ArrayList<Social>(){{
            add(new Social(1,1,"Evelyn","gato", "The tune on the radio reminds me of my childhood."));
            add(new Social(2,1,"Monce","Can", "hoy la comida estaba super"));
            add(new Social(3,2,"Monic","pato", "Mi plato preferido"));
            add(new Social(4,3,"Sebas","Sebas", "Con mi familia"));
        }};
    }

}
