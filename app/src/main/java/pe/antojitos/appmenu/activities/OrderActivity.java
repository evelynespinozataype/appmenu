package pe.antojitos.appmenu.activities;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.ArrayList;

import pe.antojitos.appmenu.R;
import pe.antojitos.appmenu.adapters.OrderAdapter;
import pe.antojitos.appmenu.models.Order;

public class OrderActivity extends AppCompatActivity {

    private OrderAdapter orderAdapter;
    private RecyclerView.LayoutManager layoutManager;
    ArrayList<Order> lstOrder;

    Dialog msgDialog;
    Button btnAcept;
    TextView txtMsg;
    ImageView btnImgClose;
    EditText txtCode;

    //private ZXingScannerView mScannerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        msgDialog = new Dialog(this);
        setToolbar();
        BuildRecyclerView();
        btnPay_onclick();
        btnAdd_onclick();

        /*Habilitar CONTEXT MENU
        registerForContextMenu(findViewById(R.id.order_pay_id));*/

    }

    //region METHOD: READER QR
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
        if(result != null){
            if(result.getContents() != null){
                showPopup(result.getContents());
                Toast.makeText(this, "El codigo de barras es: " + result.getContents(), Toast.LENGTH_LONG).show();
            }else{
                showPopup("Error escaner");
                Toast.makeText(this, "Error al escanear", Toast.LENGTH_SHORT).show();
            }
        }

    }

    public void readerCodeQR() {
        new IntentIntegrator(this).initiateScan();

    }
    //endregion

    public void showPopup(String code){

        msgDialog.setContentView(R.layout.popup);
        btnAcept = (Button) msgDialog.findViewById(R.id.popup_btn_acept_id);
        btnImgClose = (ImageView) msgDialog.findViewById(R.id.popup_btnImg_close_id);
        txtMsg = (TextView) msgDialog.findViewById(R.id.popup_msg_id);
        txtCode = (EditText) msgDialog.findViewById(R.id.popup_code_id);
        this.txtCode.setText(code);
        this.txtCode.setVisibility(EditText.INVISIBLE);

        btnAcept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                msgDialog.dismiss();

                //Toast.makeText(this,"CODE", Toast.LENGTH_LONG).show();
                //btnCodeQR();
            }
        });
        btnImgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                msgDialog.dismiss();
            }
        });

        msgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        msgDialog.show();
    }

    public void BuildRecyclerView(){

        lstOrder = this.getAllOrders(); //Llena la lista con los datos de los platos
        RecyclerView myrv = (RecyclerView) findViewById(R.id.order_recyclerview_id);
        orderAdapter = new OrderAdapter(lstOrder, R.layout.item_order, this, new OrderAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                //changeItem(position);
            }

            @Override
            public void onDeleteClick(int position) {
                removeItem(position);
            }

            @Override
            public void onAddClick(int position) {
                changeItem(position);
            }
        });
        myrv.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        myrv.setLayoutManager(layoutManager);
        myrv.setAdapter(orderAdapter);
    }

    public void insertItem(int position){
        //lstOrder.add(position, )
    }

    public void removeItem(int position){

        if(lstOrder.get(position).getQuantity()==1){
            lstOrder.remove(position);
            orderAdapter.notifyItemRemoved(position);
        }
        else{
            lstOrder.get(position).changeQuantity("MINUS");
            orderAdapter.notifyItemChanged(position);
        }

    }

    public void changeItem(int position){
        lstOrder.get(position).changeQuantity("ADD");
        orderAdapter.notifyItemChanged(position);
    }

    public ArrayList<Order> getAllOrders(){
        Intent intent = getIntent();
        lstOrder = (ArrayList<Order>) getIntent().getSerializableExtra("lstOrder");
        return lstOrder;
    }

    public void setToolbar(){
        Toolbar mytoolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(mytoolbar);
    }

    public void btnPay_onclick(){
        FloatingActionButton floatingActionButton = findViewById(R.id.order_pay_id);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear_SharePreferences();
                //showPopup();
                readerCodeQR();
                //Intent intent = new Intent(v.getContext(), MainActivity.class);
                //v.getContext().startActivity(intent);

                /*Habilitar CONTEXT MENU
                openContextMenu(v);*/
            }
        });
    }

    public void btnAdd_onclick(){
        FloatingActionButton floatingActionButton = findViewById(R.id.order_add_id);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MainActivity.class);
                v.getContext().startActivity(intent);
            }
        });
    }

    public void clear_SharePreferences(){
        SharedPreferences preferencia = getSharedPreferences("data_order", Context.MODE_PRIVATE);
        preferencia.edit().remove("order").commit();
        //preferencia.edit().clear().commit();
        lstOrder.clear();
    }

    /*
    public void btnCodeQR(){
            mScannerView = new ZXingScannerView(this);
            setContentView(mScannerView);
            mScannerView.setResultHandler(this);
        mScannerView.startCamera();

    }

    @Override
    public void handleResult(Result result) {

        Log.v("HandleResult",result.getText());
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Resultado del scanner");
        builder.setMessage(result.getText());
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        mScannerView.resumeCameraPreview(this);
    }
    */


    //region CONETXT MENU

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.context_menu,menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId()){
            case R.id.contxtmenu_separar_id:
                Toast.makeText(this,"PEDIDO SEPARADO", Toast.LENGTH_LONG).show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }



    }

    //endregion
}
