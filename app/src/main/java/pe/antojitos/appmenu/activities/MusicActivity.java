package pe.antojitos.appmenu.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.ArrayList;

import pe.antojitos.appmenu.R;
import pe.antojitos.appmenu.adapters.MusicAdapter;
import pe.antojitos.appmenu.models.Music;

public class MusicActivity extends AppCompatActivity {

    private MusicAdapter musicAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<Music> lstMusic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music);

        BuildRecyclerView();
        setToolbar();
    }

    public void BuildRecyclerView(){

        lstMusic = this.getAllMusics();
        RecyclerView myrv = (RecyclerView) findViewById(R.id.music_recyclerview_id);
        musicAdapter = new MusicAdapter(lstMusic, R.layout.item_music, this, new MusicAdapter.OnItemClickListener() {

            @Override
            public void onItemFavoriteClick(int position) {

            }

            @Override
            public void onItemPlayClick(int position) {

            }
        });
        myrv.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        myrv.setLayoutManager(layoutManager);
        myrv.setAdapter(musicAdapter);
    }

    public void setToolbar(){
        Toolbar mytoolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(mytoolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true); //Boton para retroceder al menu principal

    }
    //region METHODS: menu_bar

    private ArrayList<Music> getAllMusics(){
        return new ArrayList<Music>(){{
            add(new Music(1,"Guns and Roses","Rain", 1,1,1,R.drawable.music_arequipapareja));
            add(new Music(2,"Coldplay","Yellow", 0,0,2,R.drawable.music_arequipenia));
            add(new Music(3,"Coldplay","Real love", 1,0,3,R.drawable.music_criolla));
            add(new Music(4,"Coldplay","Real love", 1,0,4,R.drawable.music_gianmarco));
            add(new Music(5,"Coldplay","Real love", 0,0,5,R.drawable.music_lucho));
            add(new Music(6,"Coldplay","Real love", 1,0,0,R.drawable.music_otros));
            add(new Music(7,"Coldplay","Real love", 1,0,0,R.drawable.music_zambocavero));
        }};
    }



}
