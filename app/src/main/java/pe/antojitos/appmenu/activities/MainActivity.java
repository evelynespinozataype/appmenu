package pe.antojitos.appmenu.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import pe.antojitos.appmenu.FoodsWS;
import pe.antojitos.appmenu.ApiWSAntojitos;
import pe.antojitos.appmenu.R;
import pe.antojitos.appmenu.adapters.ViewPagerAdapter;
import pe.antojitos.appmenu.models.Food;
import pe.antojitos.appmenu.models.FoodTest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "evelyn";
    Dialog marketingDialog;
    Dialog msgDialog;
    ImageView txtClose;
    Button btnClose;

    /**/
    Button btnAcept;
    TextView txtMsg;
    ImageView btnImgClose;
    EditText txtCode;


    private TextView txtvResult;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter pageAdapter;

    private int result;
    TextToSpeech toSpeetch;
    String response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        marketingDialog = new Dialog(this);
        msgDialog = new Dialog(this);
        setToolbar();
        setTabLayout();
        setViewPager();
        setListenerTabLayout(viewPager);
        iniReadText();
        setBottonNavigation();
        getAllFoodsWS();
        //showPopup();
        //showMarketingDialog();

    }

    public void showPopup(){

        msgDialog.setContentView(R.layout.popup);
        btnAcept = (Button) msgDialog.findViewById(R.id.popup_btn_acept_id);
        btnImgClose = (ImageView) msgDialog.findViewById(R.id.popup_btnImg_close_id);
        txtMsg = (TextView) msgDialog.findViewById(R.id.popup_msg_id);
        txtCode = (EditText) msgDialog.findViewById(R.id.popup_code_id);

        btnAcept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                msgDialog.dismiss();

                //Toast.makeText(this,"CODE", Toast.LENGTH_LONG).show();
                //btnCodeQR();
            }
        });
        btnImgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                msgDialog.dismiss();
            }
        });

        msgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        msgDialog.show();
    }

    public void showMarketingDialog(){

        marketingDialog.setContentView(R.layout.marketing_popup);
        txtClose = (ImageView)marketingDialog.findViewById(R.id.popup_btnImg_close_id);
        btnClose=(Button) marketingDialog.findViewById(R.id.popup_mkt_btn_acept_id);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                marketingDialog.dismiss();
            }
        });

        txtClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                marketingDialog.dismiss();
            }
        });

        marketingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        marketingDialog.show();
    }

    //region METHODS: SPEECH_TO_TEXT
    public void getSpeechInput() {

        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        Locale locSpanish = new Locale("spa", "MEX");
        //intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "es-ES");

        if(intent.resolveActivity(getPackageManager()) != null){
            startActivityForResult(intent, 10);
        }else{
            Toast.makeText(this,"Your device do not suport speech input", Toast.LENGTH_SHORT).show();
        }

        startActivityForResult(intent,10);

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data  ){
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case 10:
                if (resultCode == RESULT_OK && data != null){
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String stringResult = result.get(0);//El resultado del texto se almacena en esta variable
                    //processText(stringResult);
                    String answerText = "monga";//returnAnswerText();
                    responseRobox(answerText);//Manda para que el RObox hable
                    Toast.makeText(this,stringResult,  Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }
    //endregion

    //region METHODS: ASSISTENT SPEAK-ROBOX (READ TEXT)
    public void iniReadText(){

        //Inicializa para leer un text escrito

        toSpeetch = new TextToSpeech(MainActivity.this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status == TextToSpeech.SUCCESS){
                    Locale locSpanish = new Locale("spa", "MEX");
                    //result = toSpeetch.setLanguage(Locale.getDefault());
                    result = toSpeetch.setLanguage(locSpanish);
                }
                else{
                    Toast.makeText(MainActivity.this, "Feature not support in your device", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void responseRobox(String reponse){
        if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED){
            Toast.makeText(getApplicationContext(), "Not support", Toast.LENGTH_SHORT).show();
        }
        else{
            response = "Hola, soy Clarita, hoy tenemos de menuu: lomo saltado, arroz a la cubana, arroz con pollo..." +
                    "Que plato te gustaria servirte?";
            //toSpeetch.speak(text, TextToSpeech.QUEUE_FLUSH, null);
            toSpeetch.speak(response, TextToSpeech.QUEUE_ADD, null);
        }

    }
    //endregion

    //region METHODS: TOOLBAR, TABLAYOUT, VIEWPAGER

    public void setBottonNavigation(){
        BottomNavigationView bottomNav = findViewById(R.id.my_bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(navListener);
    }

    public void setToolbar(){
        Toolbar mytoolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(mytoolbar);

        //mytoolbar.setLogo(R.drawable.logo_antojo_small);
        //getSupportActionBar().setTitle("Clarita");
        //mytoolbar.setSubtitle("momo");
    }
    public void setTabLayout(){
        tabLayout = (TabLayout) findViewById(R.id.my_tabLayout);
        tabLayout.addTab(tabLayout.newTab().setText("Platos"));
        tabLayout.addTab(tabLayout.newTab().setText("Postres"));
        tabLayout.addTab(tabLayout.newTab().setText("Bebidas"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
    }
    public void setViewPager(){
        viewPager = (ViewPager) findViewById(R.id.my_viewPager);
        pageAdapter = new ViewPagerAdapter(getSupportFragmentManager(),tabLayout.getTabCount());
        viewPager.setAdapter(pageAdapter);
        viewPager.addOnPageChangeListener( new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }

    public void setListenerTabLayout(final ViewPager viewPager){
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                viewPager.setCurrentItem(position);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
    //endregion

    //region METHODS: menu_bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menubar,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_item_person_id:
                Intent intent = new Intent(this, ProfileActivity.class);
                this.startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    //endregion

    //region
    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    switch (menuItem.getItemId()){
                        case R.id.nav_home_id:
                            break;
                        case R.id.nav_song_id:
                            Intent intent = new Intent(getApplicationContext() , MusicActivity.class);
                            startActivity(intent);
                            break;
                        case R.id.nav_robot_id:
                            getSpeechInput();
                            break;
                        case R.id.nav_jornal_id:
                            Intent intent2 = new Intent(getApplicationContext(), SocialActivity.class);
                            startActivity(intent2);
                            break;
                    }
                    return true;
                }
            };
    //endregion


    //region METHODS: WEBSERVICE
    private List<Food> getAllFoodsWS() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiWSAntojitos.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiWSAntojitos menuWebService = retrofit.create(ApiWSAntojitos.class);

        Call<FoodsWS> requestFoodsWS = menuWebService.getAllFoods();
        requestFoodsWS.enqueue(new Callback<FoodsWS>() {

            @Override
            public void onResponse(Call<FoodsWS> call, Response<FoodsWS> response) {
                if(response.isSuccessful()){
                    //si la connecion con el servicio es exitosa
                    Log.i(TAG,"BODY : " + response.errorBody());
                    FoodsWS foodsWS = response.body();
                    Toast.makeText(MainActivity.this, foodsWS.toString(), Toast.LENGTH_SHORT).show();
                    for(Food f: foodsWS.foods){
                        Log.i("TAG",f.getTitle());
                    }
                    Log.i(TAG,"----------");
                    Toast.makeText(MainActivity.this, "WEBSERVICE", Toast.LENGTH_SHORT).show();
                    //return foodsWS;
                }else{
                    Log.i(TAG,"Responce error:"+response.code()+"BODY : " + response.errorBody());
                    Toast.makeText(MainActivity.this, "ERROR WEBSERVICE", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<FoodsWS> call, Throwable t) {
                Log.e(TAG,"onFailure error:" +t.getMessage());
            }
        });
        return null;
    }
    //endregion
}
