package pe.antojitos.appmenu.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.ArrayList;

import pe.antojitos.appmenu.R;
import pe.antojitos.appmenu.models.Order;

public class FoodActivity extends AppCompatActivity {

    private TextView det_title;
    private TextView det_description;
    private TextView det_category;
    private ImageView det_img;
    ArrayList<Order> lstOrder;

    private int orderCode;
    private String orderTitle;
    private String orderDescription;
    private int orderQuantity;
    private double orderPrice;
    private double orderTotalPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food);

        setToolbar();
        setItemView();
        btnOrder_onclick();


    }

    public void setToolbar(){
        Toolbar mytoolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(mytoolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void setItemView(){
        det_title = (TextView) findViewById(R.id.food_det_title_id);
        det_category = (TextView) findViewById(R.id.food_det_category_id);
        det_description = (TextView) findViewById(R.id.food_det_description_id);
        det_img = (ImageView) findViewById(R.id.food_det_image_id);

        //receive data
        Intent intent = getIntent();
        orderCode = intent.getExtras().getInt("code");
        orderTitle = intent.getExtras().getString("title");
        orderDescription = intent.getExtras().getString("description");
        orderQuantity = intent.getExtras().getInt("stock");
        orderPrice = intent.getExtras().getDouble("price");
        int image = intent.getExtras().getInt("image");
        String category = intent.getExtras().getString("category");

        //set data to view
        this.det_title.setText(orderTitle);
        this.det_category.setText(category);
        this.det_description.setText(orderDescription);
        this.det_img.setImageResource(image);
        Picasso.get().load(image).fit().into(det_img);
        //img.setImageResource(image);
    }

    public void btnOrder_onclick(){

        //Icono Boton para adicionar
        FloatingActionButton floatingActionButton = findViewById(R.id.btnAddFood_id);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lstOrder = new ArrayList<>();
                lstOrder.add(new Order(orderCode, orderTitle, orderDescription, 1, orderPrice, 0));
                ArrayList<Order> tmpOrders = loadData();

                if (tmpOrders != null) {
                    for (int i = 0; i < tmpOrders.size(); i++) {
                        if (lstOrder.get(0).getCode() != tmpOrders.get(i).getCode()) {
                            lstOrder.add(tmpOrders.get(i));
                        } else {
                            lstOrder.get(0).changeQuantity("ADD");
                        }
                    }
                }

                sabeData(lstOrder);
                Intent intent = new Intent(v.getContext(), OrderActivity.class);
                intent.putExtra("lstOrder", lstOrder);
                v.getContext().startActivity(intent);
            }
        });
    }

    private void sabeData(ArrayList<Order> mOrders){
        SharedPreferences preferencia = getSharedPreferences("data_order", Context.MODE_PRIVATE);
        SharedPreferences.Editor objEdit = preferencia.edit();
        Gson gson = new Gson();
        String json = gson.toJson(mOrders);
        objEdit.putString("order", json);
        objEdit.apply();
    }

    private ArrayList<Order> loadData(){
        SharedPreferences preferencia = getSharedPreferences("data_order", Context.MODE_PRIVATE);
        String json = preferencia.getString("order", null);
        Type type = new TypeToken<ArrayList<Order>>(){}.getType();
        Toast.makeText(this,"JSON LOAD "+ json, Toast.LENGTH_LONG).show();
        Gson gson = new Gson();
        ArrayList<Order> recoverOrders = gson.fromJson(json,type);

        if(recoverOrders == null){
            //recoverOrders = new ArrayList<Order>();

            Toast.makeText(this,"Objeto vacio", Toast.LENGTH_LONG).show();
        }
        return recoverOrders;

    }


}
