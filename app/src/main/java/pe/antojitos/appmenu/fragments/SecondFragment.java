package pe.antojitos.appmenu.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import pe.antojitos.appmenu.R;
import pe.antojitos.appmenu.adapters.FoodAdapter;
import pe.antojitos.appmenu.models.Food;

/**
 * A simple {@link Fragment} subclass.
 */
public class SecondFragment extends Fragment {

    private FoodAdapter foodAdapter;
    private List<Food> lstFoodDeserts;

    public SecondFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_second, container, false);
        RecyclerView myrv = (RecyclerView) view.findViewById(R.id.recyclerview_food_deserts_id);
        lstFoodDeserts = this.getAllDesertsFoods(); //Llena la lista con los datos de los platos

        foodAdapter = new FoodAdapter(lstFoodDeserts, R.layout.item_food, this.getContext(), new FoodAdapter.OnItemClickListener() {
            @Override  //OnItemClickListener es una interfaz creado en el FoodAdapter
            public void onItemClick(Food food, int position) {
                foodAdapter.notifyItemChanged(position);
            }
        });
        myrv.setHasFixedSize(true);
        //layoutManager = new LinearLayoutManager(this);
        myrv.setLayoutManager(new GridLayoutManager(this.getContext(),3));
        myrv.setAdapter(foodAdapter);

        return view;
    }

    private List<Food> getAllDesertsFoods(){
        return new ArrayList<Food>(){{
            add(new Food(31,"Pan Arabe", "Postre","Una delicia, disfrútalo.",R.drawable.food_panarabe, 10,10));
            add(new Food(32,"Torta de manzana", "Postre","Descripcion Arroz con Pollo",R.drawable.food_quequemanzana,10,10));
            add(new Food(33,"Torta de Chocolate y canihua de cacao puro", "Postre","Descripcion Ocopa",R.drawable.food_quequechoclate,10,10));
            add(new Food(34,"Muss de Limón", "Postre","Descripcion Ocopa",R.drawable.food_musslimon,10,10));
            add(new Food(35,"Muss de Fresa Super Natural", "Postre","Descripcion Ocopa",R.drawable.food_mussfresalight,10,10));
            add(new Food(36,"Muss de Fresa", "Postre","Descripcion Ocopa",R.drawable.food_mussfresa,10,10));
        }};
    }

}
