package pe.antojitos.appmenu.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import pe.antojitos.appmenu.R;
import pe.antojitos.appmenu.adapters.FoodAdapter;
import pe.antojitos.appmenu.models.Food;

public class FirstFragment extends Fragment {

    private static final String TAG = "evelyn";

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private FoodAdapter foodAdapter;
    private List<Food> lstFood;

    public FirstFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_first, container, false);
        RecyclerView myrv = (RecyclerView) view.findViewById(R.id.recyclerview_id_food);
        lstFood = this.getAllFoods(); //Llena la lista con los datos de los platos

        foodAdapter = new FoodAdapter(lstFood, R.layout.item_food, this.getContext(), new FoodAdapter.OnItemClickListener() {
            @Override  //OnItemClickListener es una interfaz creado en el FoodAdapter
            public void onItemClick(Food food, int position) {
                //food.addQuantity(1);
                foodAdapter.notifyItemChanged(position);
            }
        });
        myrv.setHasFixedSize(true);
        //layoutManager = new LinearLayoutManager(this);
        myrv.setLayoutManager(new GridLayoutManager(this.getContext(),3));
        myrv.setAdapter(foodAdapter);

        //getAllFoodsWS();
        return view;

    }

    private List<Food> getAllFoods(){
        return new ArrayList<Food>(){{
            add(new Food(1,"Lomo Saltado", "Plato Principal","El Lomo Saltado es un plato típico de la gastronomía del Perú. El plato surge por la influencia de los chinos y contiene la sazón y la mezcla de la cocina criolla peruana con aquella oriental. \n" +
                    "Los ingredientes para preparar este delicioso plato son carne de res, sal, pimienta, comino, cebolla, ajo, ají verde, vinagre tinto o blanco, tomate, perejil, papas y aceite. A veces el sillao y un chorrito de pisco son el toque perfecto para poder llevarte a la boca un trozo de este delicioso manjar. Las papas que suelen acompañar este plato deben estar fritas o sancochadas.\n" +
                    "Si en algún momento quieres preparar este delicioso plato en casa; es muy fácil. Primero tienes que cortar la carne de res en tiras medianas y sazonarla con sal, pimienta, ajos y sillao. Luego tienes que dejarla reposar unos minutos para que los ingredientes sean absorbidos.\n" +
                    "Después tienes que cortar las papas y freírlas en aceite bien caliente, cortar la cebolla y el tomate en tiras. Luego calentar el aceite en una sartén por unos minutos a fuego alto, añadir la carne y dejarla cocer por unos minutos. Una vez cocida la carne debes de adicionarle cebolla y tomate; moviendo constantemente los ingredientes para evitar que estos se puedan quemar.\n" +
                    "Agregar con mucho cuidado un chorrito de vinagre y por ultimo separar la sartén del fuego. Existen distintas formas de servir el lomo saltado.\n" +
                    "1) Mezclar el último preparado de la carne con las papas fritas, adicionándole el perejil picado.\n" +
                    "2) Colocar el preparado de la carne encima de las papas fritas.\n" +
                    "3) Servir por separado las papas y la carne.\n" +
                    "Este último suele ser el más utilizado por los peruanos en general y se poder servir acompañado de arroz blanco. Una delicia, disfrútalo.\n",R.drawable.lomosaltado, 10,10));
            add(new Food(2,"Arroz con Pollo", "Plato Principal","Descripcion Arroz con Pollo",R.drawable.arrozconpollo,10,10));
            add(new Food(3,"Ocopa Arequipeña", "Plato Principal","Descripcion Ocopa",R.drawable.ocopa,10,10));
            add(new Food(4,"Pallar con Chuleta", "Plato Principal","Descripcion Ocopa",R.drawable.food_pallar,10,10));
            add(new Food(5,"Arroz Chaufa", "Plato Principal","Descripcion Ocopa",R.drawable.food_chaufa,10,10));
            add(new Food(6,"Revuelto de Verduras", "Plato Principal","Descripcion Ocopa",R.drawable.food_revuelto,10,10));
            add(new Food(7,"Combinado de Arroz con Pollo y Tallarines con Ocopa", "Plato Principal","Descripcion Ocopa",R.drawable.food_arroztallarin,10,10));
            add(new Food(8,"Arroz con Pollo", "Plato Principal","Descripcion Ocopa",R.drawable.food_arrozpollo,10,10));
            add(new Food(9,"Segundo de Lisas", "Plato Principal","Descripcion Ocopa",R.drawable.food_lisas,10,10));
            add(new Food(10,"Causa Rellena", "Plato Principal","Descripcion Ocopa",R.drawable.food_causa,10,10));
            add(new Food(11,"Ceviche", "Plato Principal","Descripcion Ocopa",R.drawable.food_seviche,10,10));
            add(new Food(12,"Aji de Gallina", "Plato Principal","Descripcion Ocopa",R.drawable.food_ajigallina,10,10));
        }};
    }

}
