package pe.antojitos.appmenu.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import pe.antojitos.appmenu.R;
import pe.antojitos.appmenu.adapters.FoodAdapter;
import pe.antojitos.appmenu.models.Food;

/**
 * A simple {@link Fragment} subclass.
 */
public class ThirdFragment extends Fragment {

    private FoodAdapter foodAdapter;
    private List<Food> lstFoodDrinks;

    public ThirdFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_third, container, false);
        RecyclerView myrv = (RecyclerView) view.findViewById(R.id.recyclerview_food_drinks_id);
        lstFoodDrinks = this.getAllDrinksFoods(); //Llena la lista con los datos de los platos

        foodAdapter = new FoodAdapter(lstFoodDrinks, R.layout.item_food, this.getContext(), new FoodAdapter.OnItemClickListener() {
            @Override  //OnItemClickListener es una interfaz creado en el FoodAdapter
            public void onItemClick(Food food, int position) {
                foodAdapter.notifyItemChanged(position);
            }
        });
        myrv.setHasFixedSize(true);
        //layoutManager = new LinearLayoutManager(this);
        myrv.setLayoutManager(new GridLayoutManager(this.getContext(),3));
        myrv.setAdapter(foodAdapter);

        return view;
    }

    private List<Food> getAllDrinksFoods(){
        return new ArrayList<Food>(){{
            add(new Food(31,"Chicha Morada", "Postre","Una delicia, disfrútalo.",R.drawable.food_drink_chicha, 10,10));
            add(new Food(33,"Té", "Postre","Descripcion Ocopa",R.drawable.food_drink_te,10,10));
            add(new Food(34,"Jugo verde", "Postre","Descripcion Ocopa",R.drawable.food_drink_verde,10,10));
            add(new Food(35,"Jugo de zanahoria", "Postre","Descripcion Ocopa",R.drawable.food_drink_zanahoria,10,10));
            add(new Food(32,"Jugo de veterraga", "Postre","El jugo de remolacha nos ayuda a mantener el organismo sano y con un buen rendimiento físico, es incluso mejor que cualquier bebida energética. La remolacha es un alimento con una riqueza nutricional muy importante, que aporta beneficios para la salud general. Entonces, ¿conoces las bondades de la remolacha?\n" +
                    "1. Baja la presión arterial alta:\n" +
                    "2. Mejora el rendimiento físico:\n" +
                    "3. Combate la inflamación:\n" +
                    "4. Desintoxica el cuerpo:\n" +
                    "5. Son fuente de minerales y fibra:\n" +
                    "6. Tiene propiedades anticancerígenas:\n",R.drawable.food_drink_jugoveterraga,10,10));
        }};
    }

}
