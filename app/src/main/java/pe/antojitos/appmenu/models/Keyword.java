package pe.antojitos.appmenu.models;

public class Keyword {

    int id_keyword;
    int ind_keyword;
    String nom_keyword;

    public Keyword(int id_keyword, int ind_keyword, String nom_keyword) {
        this.id_keyword = id_keyword;
        this.ind_keyword = ind_keyword;
        this.nom_keyword = nom_keyword;
    }

    public int getId_keyword() {
        return id_keyword;
    }

    public void setId_keyword(int id_keyword) {
        this.id_keyword = id_keyword;
    }

    public int getInd_keyword() {
        return ind_keyword;
    }

    public void setInd_keyword(int ind_keyword) {
        this.ind_keyword = ind_keyword;
    }

    public String getNom_keyword() {
        return nom_keyword;
    }

    public void setNom_keyword(String nom_keyword) {
        this.nom_keyword = nom_keyword;
    }
}
