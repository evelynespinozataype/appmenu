package pe.antojitos.appmenu.models;

import java.io.Serializable;

/* Serializable is a standard Java and Parcelable is a Android specific interface
* to pass the objects to activities and save in Disk (Memory)
* https://acadgild.com/blog/introduction-serializable-parcelable
* */
public class Order  implements Serializable {


    private int code;
    private String name;
    private String description;
    private int quantity;
    private double price;
    private double totalPrice;

    public Order(int code, String name, String description, int quantity, double price, double totalPrice) {
        this.code = code;
        this.name = name;
        this.description = description;
        this.quantity = quantity;
        this.price = price;
        this.totalPrice = totalPrice;
    }

    public void changeQuantity(String operation){
        if(operation == "ADD"){
            quantity = quantity + 1;
            totalPrice = totalPrice + price;
        }
        if(operation == "MINUS"){
            quantity = quantity - 1;
            totalPrice = totalPrice - price;
        }

    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }
}
