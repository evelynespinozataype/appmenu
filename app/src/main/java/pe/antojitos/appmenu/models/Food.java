package pe.antojitos.appmenu.models;

public class Food {

    private int code;
    private String title;
    private String category;
    private String description;
    private int Thumbnail;
    private int stock;
    private double price;

    public static final int LIMIT_QUANTITY = 10;
    public static final int RESET_VALUE_QUANTITY = 0;

    // Añadir cantidad

    public void addQuantity(int quantity) {
        if (this.stock < LIMIT_QUANTITY)
            this.stock += quantity;
    }

    public Food(int code, String title, String category, String description, int thumbnail, int stock, double price) {
        this.code = code;
        this.title = title;
        this.category = category;
        this.description = description;
        Thumbnail = thumbnail;
        this.stock = stock;
        this.price = price;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getThumbnail() {
        return Thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        Thumbnail = thumbnail;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
