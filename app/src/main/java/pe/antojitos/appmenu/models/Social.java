package pe.antojitos.appmenu.models;

public class Social {

    private int id;
    private int state;
    private String name;
    private String nickname;
    private String comment;

    public Social(int id, int state, String name, String nickname, String comment) {
        this.id = id;
        this.state = state;
        this.name = name;
        this.nickname = nickname;
        this.comment = comment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getComment() {
        return comment;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
