package pe.antojitos.appmenu.models;

public class Music {

    private int music_id;
    private String autor;
    private String song;
    private int favorite;
    private int isPlay;
    private int priority;
    private int image;

    public Music(int music_id, String autor, String song, int favorite, int isPlay, int priority, int image) {
        this.music_id = music_id;
        this.autor = autor;
        this.song = song;
        this.favorite = favorite;
        this.isPlay = isPlay;
        this.priority = priority;
        this.image = image;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getSong() {
        return song;
    }

    public void setSong(String song) {
        this.song = song;
    }

    public int getFavorite() {
        return favorite;
    }

    public void setFavorite(int favorite) {
        this.favorite = favorite;
    }

    public int getIsPlay() {
        return isPlay;
    }

    public void setIsPlay(int isPlay) {
        this.isPlay = isPlay;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getMusic_id() {
        return music_id;
    }

    public void setMusic_id(int music_id) {
        this.music_id = music_id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
