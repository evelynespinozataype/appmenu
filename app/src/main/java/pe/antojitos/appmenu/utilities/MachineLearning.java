package pe.antojitos.appmenu.utilities;

import java.util.ArrayList;
import java.util.List;

import pe.antojitos.appmenu.models.Keyword;

public class MachineLearning {

    private String text;

    public void standardsTextDialog(String text){
        String answer = null;
        String saludo = "Hola, soy Clarita";
        String opcionesPlatos = "hoy tenemos los siguientes platos: ";
        String opcionesPostres = "hoy tenemos los siguientes postres: ";
        String opcionesBebidas = "hoy tenemos las siguientes nbebidas: ";
        String preguntaOpcionesPlato = "Que plato te gustaria servirte?";
        String preguntaOpcionesPostre = "Que postre te gustaria servirte?";
        String preguntaOpcionesBebida = "Que bebida te gustaria servirte?";
        String confirmadoPlato = "Confirmado: ";
        String confirmadoPostre = "Confirmado de Postre: ";
        String confirmadoBebida = "Confirmado de Bebida: ";
        String sugerenciaPlato = "Hoy tenemos para sugerirte : ";
        String sugerenciaPostre = "Hoy tenemos para sugerirte de postre: ";
        String sugerenciaBebida = "Hoy tenemos para sugerirte de bebida: ";
        String resumenPedido = "Resumen de pedido: ";
        String despedida1 = "listo, tu orden ya esta en camino";
        String despedida2 = "listo, tu orden ya esta en camino, buen apetito.";

    }

    public void keyWords(){

        ArrayList<Keyword> keyWords = new ArrayList<>();
        keyWords.add(new Keyword(1,1,"aburrido"));
        keyWords.add(new Keyword(1,2,"pena"));
        keyWords.add(new Keyword(1,3,"feo"));
        keyWords.add(new Keyword(1,4,"horrible"));
        keyWords.add(new Keyword(1,5,"asco"));
        keyWords.add(new Keyword(1,6,"no me gusta"));
        keyWords.add(new Keyword(1,7,"pesimo"));
        keyWords.add(new Keyword(2,8,"muy bien"));
        keyWords.add(new Keyword(2,9,"simpatico"));
        keyWords.add(new Keyword(2, 10,"bonito"));
        keyWords.add(new Keyword(2,11,"lindo"));
        keyWords.add(new Keyword(2,12,"nice"));
        keyWords.add(new Keyword(2,13,"emocion"));
        keyWords.add(new Keyword(2,14,"maravilloso"));
        keyWords.add(new Keyword(2,15,"delicioso"));
        keyWords.add(new Keyword(2,16,"locura"));
    }


}
