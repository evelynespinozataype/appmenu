package pe.antojitos.appmenu;

import java.util.List;

import pe.antojitos.appmenu.models.Food;
import pe.antojitos.appmenu.models.Note;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiWSAntojitos {

    public static final String BASE_URL = "http://localhost:3000/api/";

    @GET("foods")
    Call<FoodsWS> getAllFoods();

    @GET("foods")  //http://localhost:3000/api/notes/
    Call<List<Food>> getFoodService(@Query("id") String user);


}
