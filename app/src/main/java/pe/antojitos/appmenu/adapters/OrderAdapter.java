package pe.antojitos.appmenu.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import pe.antojitos.appmenu.R;
import pe.antojitos.appmenu.models.Order;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder>{

    private ArrayList<Order> mDataOrder;
    private int layout;
    private Activity activity;
    private OnItemClickListener mListener;

    //region INTERFAZ
    public interface OnItemClickListener {
        void onItemClick(int position);
        void onDeleteClick(int position);
        void onAddClick(int position);
    }
    //endregion

    public OrderAdapter(ArrayList<Order> mDataOrder, int layout, Activity activity, OnItemClickListener listener) {
        this.mDataOrder = mDataOrder;
        this.layout = layout;
        this.activity = activity;
        this.mListener = listener;
    }

    //region METHODS: LIST VIEW_HOLDER
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(activity).inflate(layout,viewGroup,false);
        ViewHolder vh = new ViewHolder(v, mListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.bind(mDataOrder.get(position));
    }

    @Override
    public int getItemCount() {
        if(mDataOrder != null)
            return mDataOrder.size();
        else
            return 0;
    }
    //endregion

    //region METHODS: Item ViewHolder
    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView order_name_text;
        TextView order_description_text;
        TextView order_quantity_text;
        TextView order_price_text;
        ImageView order_deleteOnclick_img;
        ImageView order_addOnClick_img;

        public ViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);

            order_name_text = (TextView) itemView.findViewById(R.id.order_name_text_id);
            order_description_text = (TextView) itemView.findViewById(R.id.order_descrip_text_id);
            order_quantity_text = (TextView) itemView.findViewById(R.id.order_quantity_text_id);
            order_price_text = (TextView) itemView.findViewById(R.id.order_price_text_id);
            order_deleteOnclick_img = (ImageView) itemView.findViewById(R.id.order_img_delete);
            order_addOnClick_img = (ImageView) itemView.findViewById(R.id.order_img_add);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mListener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            mListener.onItemClick(position);
                        }
                    }
                }
            });

            order_deleteOnclick_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mListener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            mListener.onDeleteClick(position);
                        }
                    }
                }
            });

            order_addOnClick_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mListener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            mListener.onAddClick(position);
                        }
                    }
                }
            });
        }

        public void bind(final Order order){
            this.order_name_text.setText(order.getName());
            this.order_description_text.setText(order.getDescription());
            this.order_quantity_text.setText(String.valueOf(order.getQuantity()));
            this.order_price_text.setText(String.valueOf(order.getTotalPrice()));
        }

    }
    //endregion


}
