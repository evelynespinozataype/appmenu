package pe.antojitos.appmenu.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import pe.antojitos.appmenu.R;
import pe.antojitos.appmenu.models.Social;

public class SocialAdapter extends RecyclerView.Adapter<SocialAdapter.ViewHolder>{

    private ArrayList<Social> mDataSocial;
    private int layout;
    private Activity activity;
    private OnItemClickListener mListener;

    //region INTERFAZ
    public interface OnItemClickListener {
        void onItemClick(int position);
    }
    //endregion

    public SocialAdapter(ArrayList<Social> mDataSocial, int layout, Activity activity) {
        this.mDataSocial = mDataSocial;
        this.layout = layout;
        this.activity = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(activity).inflate(layout,viewGroup,false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull SocialAdapter.ViewHolder viewHolder, int position) {
        viewHolder.bind(mDataSocial.get(position));
    }

    @Override
    public int getItemCount() {
        if(mDataSocial != null)
            return mDataSocial.size();
        else
            return 0;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView img_state;
        TextView txt_name;
        TextView txt_comment;
        ImageView img_comment;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_state = (ImageView) itemView.findViewById(R.id.social_img_state_id);
            txt_name = (TextView) itemView.findViewById(R.id.social_txt_name_id);
            txt_comment = (TextView) itemView.findViewById(R.id.social_txt_comment_id);
            img_comment = (ImageView) itemView.findViewById(R.id.social_img_comment_id);
        }

        public void bind(final Social social) {
            this.txt_name.setText(social.getName());
            this.txt_comment.setText(social.getComment());
        }
    }
}
