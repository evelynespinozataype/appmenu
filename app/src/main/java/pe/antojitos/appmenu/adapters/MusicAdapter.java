package pe.antojitos.appmenu.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import pe.antojitos.appmenu.R;
import pe.antojitos.appmenu.models.Music;

public class MusicAdapter extends RecyclerView.Adapter<MusicAdapter.ViewHolder>{

    private ArrayList<Music> mDataMusic;
    private int layout;
    private Activity activity;
    private OnItemClickListener mListener;

    public interface OnItemClickListener{
        void onItemFavoriteClick(int position);
        void onItemPlayClick(int position);
    }

    public MusicAdapter(ArrayList<Music> mDataMusic, int layout, Activity activity, OnItemClickListener mListener) {
        this.mDataMusic = mDataMusic;
        this.layout = layout;
        this.activity = activity;
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(activity).inflate(layout,viewGroup,false);
        MusicAdapter.ViewHolder vh = new MusicAdapter.ViewHolder(v, mListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MusicAdapter.ViewHolder viewHolder, int position) {
        viewHolder.bind(mDataMusic.get(position));
    }

    @Override
    public int getItemCount() {
        return mDataMusic.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView music_txtAutor;
        TextView music_txtSong;
        TextView music_txtPriority;
        ImageView music_btnImg_favorite;
        ImageView music_btnImg_song;
        ImageView music_img;
        CardView cardView;


        public ViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);

            music_txtAutor = (TextView) itemView.findViewById(R.id.music_txtAutor_id);
            music_txtSong = (TextView) itemView.findViewById(R.id.music_txtSong_id);
            music_txtPriority= (TextView) itemView.findViewById(R.id.music_txtPriority_id);
            music_btnImg_favorite = (ImageView) itemView.findViewById(R.id.music_btnImg_favorite_id);
            music_btnImg_song = (ImageView) itemView.findViewById(R.id.music_btnImg_song_id);
            music_img = (ImageView) itemView.findViewById(R.id.music_img_id);
            cardView = (CardView) itemView.findViewById(R.id.cardview_music_id);

            music_btnImg_favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            music_btnImg_song.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

        }

        public void bind(final Music music){
            this.music_txtAutor.setText(music.getAutor());
            this.music_txtSong.setText(music.getSong());
            this.music_txtPriority.setText(String.valueOf(music.getPriority()));
            this.music_img.setImageResource(music.getImage());
            Picasso.get().load(music.getImage()).fit().into(this.music_img);
        }
    }

}
