package pe.antojitos.appmenu.adapters;

import android.support.v7.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.List;

import pe.antojitos.appmenu.R;
import pe.antojitos.appmenu.activities.FoodActivity;
import pe.antojitos.appmenu.models.Food;

public class FoodAdapter extends RecyclerView.Adapter<FoodAdapter.ViewHolder>{


    private List<Food> mDataFoods;
    private int layout;
    //private Activity activity;
    private Context context;
    private OnItemClickListener listener;

    public FoodAdapter(List<Food> mDataFoods, int layout, Context context, OnItemClickListener listener) {
        this.mDataFoods = mDataFoods;
        this.layout = layout;
        this.context = context;
        this.listener = listener;
    }

    //region VIEW_HOLDER de la lista de platos
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(this.context).inflate(layout, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.bind(mDataFoods.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return mDataFoods.size();
    }
    //endregion

    //region VIEW_HOLDER de cada Plato

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView food_tittle;
        ImageView food_img;
        CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);
            food_tittle = (TextView) itemView.findViewById(R.id.food_title_id);
            food_img = (ImageView) itemView.findViewById(R.id.food_img_id);
            cardView = (CardView) itemView.findViewById(R.id.cardview_id);
        }

        public void bind(final Food food, final OnItemClickListener listener){

            this.food_tittle.setText(food.getTitle());
            this.food_img.setImageResource(food.getThumbnail());
            Picasso.get().load(food.getThumbnail()).fit().into(this.food_img);

            this.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(v.getContext(), "Clicked Laugh Vote", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(v.getContext(), FoodActivity.class);
                    intent.putExtra("code",food.getCode());
                    intent.putExtra("title",food.getTitle());
                    intent.putExtra("category",food.getCategory());
                    intent.putExtra("description",food.getDescription());
                    intent.putExtra("image",food.getThumbnail());
                    intent.putExtra("stock",food.getStock());
                    intent.putExtra("price",food.getPrice());
                    v.getContext().startActivity(intent);
                }
            });


        }

    }

    //endregion

    //Interfaz que es inmplementado en la FirstFragment
    public interface OnItemClickListener {
        void onItemClick(Food food, int position);
    }

}
